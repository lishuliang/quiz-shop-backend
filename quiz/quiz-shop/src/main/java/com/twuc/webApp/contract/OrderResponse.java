package com.twuc.webApp.contract;

public class OrderResponse {
    private Long id;
    private String name;
    private Double price;
    private String unit;
    private Integer quantity;
    private Long goodsId;


    public OrderResponse() {
    }

    public OrderResponse(Long id, String name, Double price, String unit, Integer quantity, Long goodsId) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.quantity = quantity;
        this.goodsId = goodsId;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public Long getId() {
        return id;
    }

    public Long getGoodsId() {
        return goodsId;
    }
}
