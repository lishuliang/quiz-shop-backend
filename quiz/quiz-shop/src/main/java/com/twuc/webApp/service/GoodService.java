package com.twuc.webApp.service;

import com.twuc.webApp.contract.GoodRequest;
import com.twuc.webApp.model.Goods;
import com.twuc.webApp.model.GoodRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoodService {

    private GoodRepository goodRepository;

    public GoodService(GoodRepository goodResposity) {
        this.goodRepository = goodResposity;
    }

    public List<Goods> getAllGood() {
        return goodRepository.findAll();
    }

    public void createGood(GoodRequest goodRequest) {
        Goods good = new Goods(goodRequest.getName(), goodRequest.getPrice(), goodRequest.getUnit(),goodRequest.getUrl());
        goodRepository.saveAndFlush(good);
    }
}
