package com.twuc.webApp.contract;

import javax.validation.constraints.NotNull;

public class GoodRequest {
    @NotNull
    private String name;
    @NotNull
    private double price;
    @NotNull
    private String unit;
    @NotNull
    private String url;

    public GoodRequest() {
    }

    public GoodRequest(@NotNull String name, @NotNull double price, @NotNull String unit, @NotNull String url) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getUrl() {
        return url;
    }
}
