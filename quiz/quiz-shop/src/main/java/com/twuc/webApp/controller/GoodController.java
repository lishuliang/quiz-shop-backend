package com.twuc.webApp.controller;

import com.twuc.webApp.contract.GoodRequest;
import com.twuc.webApp.contract.GoodResponse;
import com.twuc.webApp.model.Goods;
import com.twuc.webApp.service.GoodService;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class GoodController {

    private GoodService goodService;

    public GoodController(GoodService goodService) {
        this.goodService = goodService;
    }

    @GetMapping("/goods")
    public ResponseEntity getAllGoods() {
        List<Goods> allGood = goodService.getAllGood();
//        Link selfRel = linkTo(methodOn(GoodController.class).getAllGoods()).withSelfRel();
//        Resource<List<GoodResponse>> resource = new Resource<>(allGoodsResponse, selfRel);
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON)
//                .header("Access-Control-Allow-Credentials:true")
                .body(allGood);
    }

    @PostMapping("/goods")
    public ResponseEntity createGood(@RequestBody @Valid GoodRequest goodRequest) {
        goodService.createGood(goodRequest);
        return ResponseEntity.status(HttpStatus.CREATED)
                .contentType(MediaType.APPLICATION_JSON)
//                .location(linkTo(methodOn(GoodController.class).getAllGoods()).toUri())
                .build();
    }

}
