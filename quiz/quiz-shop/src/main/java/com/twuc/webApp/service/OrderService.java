package com.twuc.webApp.service;

import com.twuc.webApp.contract.OrderRequest;
import com.twuc.webApp.contract.OrderResponse;
import com.twuc.webApp.exception.NotExistException;
import com.twuc.webApp.model.GoodRepository;
import com.twuc.webApp.model.Goods;
import com.twuc.webApp.model.Orders;
import com.twuc.webApp.model.OrderRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class OrderService {

    private OrderRepository orderRepository;
    private GoodRepository goodRepository;

    public OrderService(OrderRepository orderRepository, GoodRepository goodRepository) {
        this.orderRepository = orderRepository;
        this.goodRepository = goodRepository;
    }

    public List<OrderResponse> getAllOrdersResponse() {
        List<Orders> allOrders = getAllOrders();
        return allOrders.stream().map(order ->
                new OrderResponse(order.getId(),order.getName(), order.getPrice(), order.getUnit(), order.getQuantity(),order.getGoods().getId()))
                .collect(Collectors.toList());
    }

    private List<Orders> getAllOrders() {
        return orderRepository.findAll();
    }


    public void createOrder(OrderRequest orderRequest) {
        Orders order = orderRepository.findByGoodsId(orderRequest.getGoodsId());
        if (order != null) {
            order.setQuantity(orderRequest.getQuantity());
            orderRepository.save(order);
        } else {
            Goods good = goodRepository.findById(orderRequest.getGoodsId()).orElseThrow(() -> new NotExistException("商品不存在"));
            Orders orders = new Orders(good.getName(), good.getPrice(), good.getUnit(), orderRequest.getQuantity(), good);
            orderRepository.save(orders);
        }
    }

    public void deleteOrder(Long id) {
        orderRepository.deleteById(id);
    }
}
