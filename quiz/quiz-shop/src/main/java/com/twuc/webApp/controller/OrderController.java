package com.twuc.webApp.controller;

import com.twuc.webApp.contract.OrderRequest;
import com.twuc.webApp.contract.OrderResponse;
import com.twuc.webApp.model.Orders;
import com.twuc.webApp.service.OrderService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class OrderController {

    private OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping("/orders")
    public ResponseEntity getAllOrders() {
        List<OrderResponse> allOrdersResponse = orderService.getAllOrdersResponse();
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(allOrdersResponse);
    }

    @PostMapping("/orders")
    public ResponseEntity createOrder(@RequestBody @Valid OrderRequest orderRequest) {
        orderService.createOrder(orderRequest);
        return ResponseEntity.status(HttpStatus.CREATED)
                .contentType(MediaType.APPLICATION_JSON)
                .build();
    }

    @DeleteMapping("/orders/{id}")
    public ResponseEntity deleteorder(@PathVariable Long id) {
        orderService.deleteOrder(id);
        return ResponseEntity.ok().build();
    }

}
