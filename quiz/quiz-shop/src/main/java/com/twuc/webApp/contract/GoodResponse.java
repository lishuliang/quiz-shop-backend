package com.twuc.webApp.contract;

import javax.validation.constraints.NotNull;

public class GoodResponse {
    private String name;
    private Double price;
    private String unit;
    private String url;

    public GoodResponse() {
    }

    public GoodResponse(String name, Double price, String unit, String url) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getUrl() {
        return url;
    }
}
