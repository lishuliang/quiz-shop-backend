package com.twuc.webApp.contract;

import javax.validation.constraints.NotNull;

public class OrderRequest {
    @NotNull
    private Long goodsId;
    @NotNull
    private Integer quantity;


    public OrderRequest() {
    }

    public OrderRequest(Long goodsId, Integer quantity) {
        this.goodsId = goodsId;
        this.quantity = quantity;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public Integer getQuantity() {
        return quantity;
    }
}
