package com.twuc.webApp.model;

import javax.persistence.*;

@Entity(name = "orders")
public class Orders {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private Double price;
    @Column(nullable = false)
    private String unit;
    @Column(nullable = false)
    private Integer quantity;
    @OneToOne
    @JoinColumn(name = "goodsId")
    private Goods goods;

    public Orders() {
    }

    public Orders(String name, Double price, String unit, Integer quantity, Goods goods) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.quantity = quantity;
        this.goods = goods;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity += quantity;
    }

    public void setGoods(Goods goods) {
        this.goods = goods;
    }

    public Goods getGoods() {
        return goods;
    }
}
