package com.twuc.webApp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.contract.GoodRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class GoodControllerTest {
    
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void should_get_all_goods() throws Exception {
        mockMvc.perform(get("/api/goods").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200));
    }

    @Test
    void should_return_true_header_when_add_good() throws Exception {
        GoodRequest goodRequest = new GoodRequest("cola",2.0,"瓶","aaa");
        String good = objectMapper.writeValueAsString(goodRequest);
        mockMvc.perform(post("/api/goods").content(good).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(201));
//                .andExpect(header().string("location","http://localhost/api/goods/1"));
    }

    @Test
    void should_return_400_when_false_good() throws Exception {
        GoodRequest goodRequest = new GoodRequest("cola",2.0,null,"aaa");
        String good = objectMapper.writeValueAsString(goodRequest);
        mockMvc.perform(post("/api/goods").content(good).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(400));
    }
}
