package com.twuc.webApp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.contract.OrderRequest;
import com.twuc.webApp.model.GoodRepository;
import com.twuc.webApp.model.Goods;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import javax.persistence.EntityManager;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
 class OrderControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void should_getAllOrders() throws Exception {
        mockMvc.perform(get("/api/orders"))
                .andExpect(status().isOk());
    }

    @Test
    void should_create_order() throws Exception {
        Goods goods = new Goods("cola", 1.0, "ping", "http://localhost:8080");
//        em.flush();
//        em.clear();
        OrderRequest orderRequest = new OrderRequest(1L,2);
        String order = objectMapper.writeValueAsString(orderRequest);
        mockMvc.perform(post("/api/orders").content(order).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(201));
    }

    @Test
    void should_add_order_when_patch() {

    }
}
